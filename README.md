# Configuration.nix

This repo contains my configuration.nix file. That's it.

### Setup Home Manager (required)

```
sudo nix-channel --add https://github.com/nix-community/home-manager/archive/release-23.05.tar.gz home-manager

sudo nix-channel --update
```

### Setup SDDM and Dotfiles

```
git clone https://gitlab.com/theshatterstone/dotfiles
git clone https://gitlab.com/theshatterstone/neovim
git clone https://gitlab.com/theshatterstone/linux-themes
git clone https://gitlab.com/theshatterstone/sddm
git clone https://gitlab.com/theshatterstone/dwm

cp -r dotfiles/.config $HOME/
cp dotfiles/.bashrc $HOME/
cp -r neovim/.config $HOME/
cp -r neovim/local $HOME/
cp -r linux-themes/.themes $HOME/
cp -r linux-themes/.icons $HOME/

cp dotfiles/.zshrc ~/
```

#### Install vim-plug Plugin Manager (for vim and neovim):

```
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
```


### !!! Important !!!

Make sure to use NixOS 23.05 or later, and run an update via
``` sudo nixos-rebuild switch --upgrade```
to ensure waybar >=0.9.18 which adds persistent workspaces,
making my Hyprland setup perfect.

Note for me: In qtile, change the shebang in autostart.sh to `#!/usr/bin/env bash` to fix autostart
