# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:
{

  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
     <home-manager/nixos> 
    ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.efi.efiSysMountPoint = "/boot";
  
  # Enable Plymouth Splash Screen
  boot.plymouth.enable = false;

  #enable Nix Flakes and the new nix-command command line tool
  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  networking.hostName = "NixOSBTW"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;
  programs.nm-applet.enable = true;  

  # Set your time zone.
  time.timeZone = "Europe/London";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_GB.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_GB.UTF-8";
    LC_IDENTIFICATION = "en_GB.UTF-8";
    LC_MEASUREMENT = "en_GB.UTF-8";
    LC_MONETARY = "en_GB.UTF-8";
    LC_NAME = "en_GB.UTF-8";
    LC_NUMERIC = "en_GB.UTF-8";
    LC_PAPER = "en_GB.UTF-8";
    LC_TELEPHONE = "en_GB.UTF-8";
    LC_TIME = "en_GB.UTF-8";
  };

  # Enable the X11 windowing system.
  services.xserver.enable = true;
  services.xserver.displayManager.sessionPackages = [
    # (pkgs.dwl.overrideAttrs
    #  (prevAttrs: rec {
    #    postInstall =
    #      let
    #        dwlSession = ''
    #          [Desktop Entry]
    #          Name=DWL
    #          Comment=Dynamic Wayland compositor
    #          Exec=/home/shatterstone/.local/bin/dwl-start
    #          Type=Application
    #        '';
    #      in
    #      ''
    #        mkdir -p $out/share/wayland-sessions
    #        echo "${dwlSession}" > $out/share/wayland-sessions/dwl.desktop
    #      '';
    #    passthru.providedSessions = [ "dwl" ];
    #  })
    #)
    #(pkgs.river.overrideAttrs
    #  (prevAttrs: rec {
    #    postInstall =
    #      let
    #        riverSession = ''
    #          [Desktop Entry]
    #          Name=River
    #          Comment=Dynamic Wayland compositor
    #          Exec=river
    #          Type=Application
    #        '';
    #      in
    #      ''
    #        mkdir -p $out/share/wayland-sessions
    #        echo "${riverSession}" > $out/share/wayland-sessions/river.desktop
    #      '';
    #    passthru.providedSessions = [ "river" ];
    #  })
    #)
    #pkgs.qtile-unwrapped
  ];

  #inputs.xmonad-contexts = {
  #url = "github:Procrat/xmonad-contexts";
  #flake = false;
  #};

  # Enable the Cinnamon Desktop Environment.
  services.xserver.displayManager.sddm.enable = true;
  #services.xserver.desktopManager.cinnamon.enable = true;
  #services.xserver.desktopManager.pantheon.enable = true;
  programs.sway.enable = true;
  services.xserver.windowManager = {
    # qtile.enable = true;
    #qtile = {
      #enable = true;
      #backend = "x11";
      #package = pkgs.qtile-unwrapped;
      #configFile = "/home/shatterstone/.config/qtile/config.py";
      #extraPackages = python3Packages: with python3Packages; [
      #  qtile-extras
      #];
    #};
    awesome.enable = false;
    bspwm.enable = false;
    openbox.enable = false;
    xmonad = {
      enable = false;
      enableContribAndExtras = false;
      config = builtins.readFile /home/shatterstone/.config/xmonad/xmonad.hs;
      ghcArgs = [
        "-hidir /tmp" # place interface files in /tmp, otherwise ghc tries to write them to the nix store
        "-odir /tmp" # place object files in /tmp, otherwise ghc tries to write them to the nix store
        #"-i${xmonad-contexts}" # tell ghc to search in the respective nix store path for the module
      ];
    };
    dwm.enable = false;
  };

  services.xserver.displayManager.defaultSession = "hyprland";
  programs.hyprland.enable = true;
  programs.hyprland.xwayland.enable = true;
  # Configure keymap in X11
  services.xserver = {
    layout = "gb";
    xkbVariant = "";
  };

  # Configure console keymap
  console.keyMap = "uk";
  
  # Set up custom SDDM theme
  services.xserver.displayManager.sddm.theme = "sugar-dark";
  # services.xserver.displayManager.sddm.themedir = "/home/shatterstone/.local/share/sddm/themes/";
  services.xserver.displayManager.sddm.settings = {
    Theme = {
      Current = "sugar-dark";
      ThemeDir = "/home/sddm-themes";
      FacesDir = "/run/current-system/sw/share/sddm/faces";
    };
  };

  # Enable sound with pipewire.
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  hardware.bluetooth.enable = true;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };
  
  # Setting up Thunar plugins
  programs.thunar.plugins = with pkgs.xfce; [
  thunar-archive-plugin
  thunar-volman
  ];
  services.gvfs.enable = true; #gvfs is required for volman and archive plugins
  
  # Enable blueman for bluetooth app
  services.blueman.enable = true;
  
  # Enable ZSH
  programs.zsh.enable = true;
  #Enable emacs
  services.emacs.enable = false;

  # Setting up KDE Connect
  programs.kdeconnect.enable = true;
  
  # Enable Waydroid
  virtualisation.waydroid.enable = false;
  # Enable Flatpak
  services.flatpak.enable = true;

  # Setting up Steam
  programs.steam = {
  enable = true;
  remotePlay.openFirewall = true; # Open ports in the firewall for Steam Remote Play
  dedicatedServer.openFirewall = true; # Open ports in the firewall for Source Dedicated Server
  };
  
  # Enable printing
  services.printing.enable = true;
  services.printing.drivers = [pkgs.hplip pkgs.gutenprint pkgs.gutenprintBin];
  services.avahi.enable = true;
  services.avahi.openFirewall = true;

  # Enable touchpad support (enabled default in most desktopManager).
  #services.xserver.libinput.naturalScrolling = true;
  #services.xserver.libinput.middleEmulation = true;
  services.xserver.libinput.touchpad.tapping = true;  
  services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.shatterstone = {
    isNormalUser = true;
    description = "shatterstone";
    shell = pkgs.zsh;
    extraGroups = [ "networkmanager" "wheel" "libvirtd" ];
    #packages = with pkgs; [
      # firefox
    #  thunderbird
    #];
  };
  users.mutableUsers = true;
  hardware.opengl.driSupport32Bit = true;

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;
  fonts.packages = with pkgs; [
    (nerdfonts.override { fonts = [ "Hack"]; })
    fira-code
    ubuntu_font_family
    hack-font
];
  
  #needed for virtualisation with libvirt
  virtualisation.libvirtd.enable = true;
  programs.dconf.enable = true;
  
  # Some programs use /etc/shells to determine if the user is a normal and not a system user,
  # so it's recommended to add the user shells to this list
  environment.shells = with pkgs; [ zsh ];

  # This should start allowing packages like pip to work properly
  programs.nix-ld.enable = true;
  
  # This should set up basic libraries needed to run binaries on nixos
  programs.nix-ld.libraries = with pkgs; [
    stdenv.cc.cc
    zlib
    fuse3
    icu
    zlib
    nss
    openssl
    curl
    expat
    # ...
  ];
  #NixOS overlay for DWM 
   nixpkgs.overlays = [
    #(final: prev: {
    #  dwm = prev.dwm.overrideAttrs (old: { src = /home/shatterstone/dwm/dwm-6.3 ;});
    #})
    #(final: prev: {
    #  dwl = prev.dwl.overrideAttrs (old: { src = /home/shatterstone/dwl/dwl ;});
    #})
    #(self: super: { 
    #  waybar = super.waybar.overrideAttrs (old: { 
    #    version = "0.9.18";
    #    src = /home/shatterstone/waybar-src ;
    #  }); 
    #})
#  (self: super: {
#    qtile-unwrapped = super.qtile-unwrapped.overrideAttrs(_: rec {
#      postInstall = let
#        qtileSession = ''
#[Desktop Entry]
#Name=Qtile Wayland
#Comment=Qtile on Wayland
#Exec=qtile start -b wayland
#Type=Application
#'';
#      in
#        ''
#mkdir -p $out/share/wayland-sessions
#echo "${qtileSession}" > $out/share/wayland-sessions/qtile.desktop
#'';
#      passthru.providedSessions = [ "qtile" ];
#    });
#  })
];

#services.xserver.displayManager.sessionPackages = [ pkgs.qtile-unwrapped ];

  location.provider = "geoclue2"; 
  services.redshift = {
    enable = true;
    brightness = {
      # Note the string values below.
      day = "1";
      night = "1";
    };
    temperature = {
      day = 5000;
      night = 5000;
    };
  };
  # List packages installed in system profile. To search, run:
  # $ nix search wget

  environment.systemPackages = with pkgs; [
    vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
    #(waybar.overrideAttrs (oldAttrs: {
    #mesonFlags = oldAttrs.mesonFlags ++ [ "-Dexperimental=true "];
    #}))
    waybar
    wget
    firefox
    floorp
    #afetch
    bemenu
    #pcmanfm
    starship
    neovim
    imagemagick
    #tdesktop
    lxappearance
    xfce.thunar
    xfce.thunar-archive-plugin
    xfce.thunar-volman
    kitty
    gammastep
    #alacritty
    conky
    #awesome
    pipecontrol
    qpwgraph
    # qtile
    vlc
    mpv
    fluent-reader
    fuse
    fuse3
    appimage-run
    qutebrowser
    neofetch
    feh
    rofi
    #wofi
    dmenu
    dwm
    dwl
    yambar
    somebar
    wayland
    wayland-protocols
    wlroots
    wlr-protocols
    git
    brightnessctl
    cmus
    #vivaldi
    qbittorrent
    thunderbird
    xfce.ristretto
    picom
    xfce.xfce4-screenshooter
    vscodium
    virt-manager
    #brave
    meld
    ranger
    #gnome.dconf-editor
    # onlyoffice-bin
    # fira-code
    # ubuntu_font_family
    # hack-font
    #noto-fonts
    #noto-fonts-emoji
    wineWowPackages.staging
    winetricks
    lutris
    qemu_full
    libvirt
    #grapejuice
    #obconf
    #menumaker
    #waybox
    #tint2
    libreoffice
    # kdeconnect
    doas
    autokey
    #pfetch
    vimPlugins.vim-plug
    discord
    # bspwm
    bsp-layout
    dunst
    polybar
    swaybg
    wayshot
    # hyprland
    autotiling
    #cmus
    wallutils
    xwayland
    killall
    xarchiver
    zip
    unzip
    openjdk
    #minetest
    #voxelands
    wayvnc
    mpv-unwrapped
    xfce.tumbler
    realvnc-vnc-viewer
    ripgrep
    fd
    #emacs29-pgtk
    #emacsPackages.vterm
    #android-studio
    eww-wayland
    j4-dmenu-desktop
    whatsapp-for-linux
    papirus-icon-theme
    papirus-folders
    dracula-theme
    dracula-icon-theme
    libsForQt5.qt5.qtgraphicaleffects 
    haskellPackages.xmobar
    python311
    python311Packages.adblock
    python310Packages.pip
    python310Packages.pipInstallHook
    python310Packages.pipBuildHook
    python310Packages.pip-api
    python310Packages.pywayland
    python310Packages.pywlroots
    python310Packages.pywlroots
    #python310Packages.qtile-extras
    networkmanagerapplet
    xdg-desktop-portal-wlr
    #(river.overrideAttrs (prevAttrs: rec {
    #  postInstall =
    #    let
    #      riverSession = ''
    #        [Desktop Entry]
    #        Name=River
    #        Comment=Dynamic Wayland compositor
    #        Exec=river
    #        Type=Application
    #      '';
    #    in
    #    ''
    #      mkdir -p $out/share/wayland-sessions
    #      echo "${riverSession}" > $out/share/wayland-sessions/river.desktop
    #    '';
    #  passthru.providedSessions = [ "river" ];
    #}))
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };
    #xdg.mime.defaultApplications = {
      #"application/pdf" = "vivaldi.desktop";
      #"images/png" = "ristretto";
      #"images/jpg" = "ristretto";
      #"images/png" = "ristretto.desktop";
      #"images/png" = "ristretto.desktop";
    #};
  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
   networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.11"; # Did you read the comment?

  home-manager.users.shatterstone = { pkgs, ... }: {
    home.packages = with pkgs; [ 
      htop 
    ];
    #xdg.mimeApps.defaultApplications = {
    #  "application/pdf" = "vivaldi.desktop";
    #  "images/png" = "ristretto.desktop";
    #  "images/jpg" = "ristretto.desktop";
    #  "images/png" = "ristretto.desktop";
    #  "images/png" = "ristretto.desktop";
    #};
    gtk = {
      enable = true;
      #font.name = "TeX Gyre Adventor 10";
      #theme = {
      #  name = "Juno";
      #  package = pkgs.juno-theme;
      #};
      cursorTheme = {
        #package = pkgs.vanilla-dmz;
        name = "Adwaita";
      };
      theme = {
      	name = "Arc-Darkest-3.38";
        #name = "Dracula";
        #package = pkgs.dracula-theme;
      };

      iconTheme = {
        name = "Sardi-Flat-Arc";
        #name = "Papirus-Dark";
        #package = pkgs.papirus-icon-theme;
        #name = "Dracula";
        #package = pkgs.dracula-icon-theme;
      };

      gtk3.extraConfig = {
      Settings = ''
        gtk-application-prefer-dark-theme=1
      '';
      };

      gtk4.extraConfig = {
      Settings = ''
        gtk-application-prefer-dark-theme=1
      '';
      };
    
    };
    home.stateVersion = "23.11";
  };

}
