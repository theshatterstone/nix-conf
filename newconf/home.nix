{ config, pkgs, lib, inputs, ... }:
let
  unstable = import inputs.unstable {
    system = pkgs.system;
    # Uncomment this if you need an unfree package from unstable.
    #config.allowUnfree = true;
  };
in
{

  #imports = [
  #./flake.nix
  #];

  #nixpkgs.overlays = [
  #  unstable-packages = final: _prev: {
  #    unstable = import inputs.nixpkgs-unstable {
  #      system = final.system;
  #      config.allowUnfree = true;
  #    };
  #  };
  #];
  # TODO please change the username & home directory to your own
  home.username = "shatterstone";
  home.homeDirectory = "/home/shatterstone";

  # link the configuration file in current directory to the specified location in home directory
  # home.file.".config/i3/wallpaper.jpg".source = ./wallpaper.jpg;

  # link all files in `./scripts` to `~/.config/i3/scripts`
  # home.file.".config/i3/scripts" = {
  #   source = ./scripts;
  #   recursive = true;   # link recursively
  #   executable = true;  # make all files executable
  # };

  # encode the file content in nix configuration file directly
  # home.file.".xxx".text = ''
  #     xxx
  # '';
  home.packages = with pkgs; [
    htop
    unstable.bemenu
    ];
  gtk = {
    enable = true;
    cursorTheme = {
      name = "Adwaita";
    };
    theme = {
      name = "Arc-Darkest-3.38";
    };
    iconTheme = {
      name = "Sardi-Flat-Arc";
    };
    gtk3.extraConfig = {
    Settings = ''
      gtk-application-prefer-dark-theme=1
    '';
    };
    gtk4.extraConfig = {
    Settings = ''
      gtk-application-prefer-dark-theme=1
    '';
    };
  };

  # This value determines the home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update home Manager without changing this value. See
  # the home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "23.11";

  # Let home Manager install and manage itself.
  programs.home-manager.enable = true;
}
